resource "aws_instance" "dev" {
  ami = "ami-00b3e95ade0a05b9b"
  instance_type = "t2.micro"
  tags = {
    Name = "emmanuel"
    Env = "Dev"
    Owner = "emmanel@jjtech.inc"
    Createdby = "Terraform"
  }
}

resource "aws_instance" "test" {
  ami = "ami-0fc9e52ba2aedb00d"
  instance_type = "t2.medium"
  tags = {
    Name = "Manny"
    Env = "Test"
    Owner = "emmanel@jjtech.inc"
    Createdby = "Terraform"
  }
}